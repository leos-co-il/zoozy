<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );
global $wp_query;
$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
$max_pages = $wp_query->max_num_pages;
$term_id = 0;
if (is_shop()) {
	$query = wc_get_page_id('shop');
} else {
	$query_curr = get_queried_object();
	$query = $query_curr;
	$term_id = $query->term_id;
}
/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

?>
<div class="top-block">
		<div class="container">
			<div class="row">
				<div class="col">
					<h1 class="block-title"><?php woocommerce_page_title(); ?></h1>
					<h2 class="block-subtitle mb-0">
						<?php
						/**
						 * Hook: woocommerce_archive_description.
						 *
						 * @hooked woocommerce_taxonomy_archive_description - 10
						 * @hooked woocommerce_product_archive_description - 10
						 */
						do_action( 'woocommerce_archive_description' );
						?>
					</h2>
				</div>
			</div>
		</div>
	</div>
<?php
echo '<div class="container cont-prods-top mt-4 mb-4">';
if ( woocommerce_product_loop() ) {
	echo '<div class="row justify-content-end"><div class="col-auto">';
	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	do_action( 'woocommerce_before_shop_loop' );
	echo '</div></div>';
	woocommerce_product_loop_start();
	echo '<div class="row justify-content-center align-items-stretch put-here-posts">';
	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}
	echo '</div>';
	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}
echo '<div>';
 if( $paged < $max_pages ) : ?>
	<div class="row justify-content-center mt-4 mb-5">
		<div class="col-auto">
			<div data-maxpages="<?= $max_pages ?>" data-paged="<?= $paged ?>"
				 class="more-link load-more-posts" data-type="product" data-term_name="product_cat" data-term="<?= !is_shop() ? $term_id : ''; ?>">
				טען עוד מוצרים
				<img src="<?= ICONS ?>arrow-down.png" alt="down-arrow">
			</div>
		</div>
	</div>
<?php endif;
/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
get_template_part('views/partials/repeat', 'offer', [
		'text' => true,
]);
if ($faq = get_field('faq_item', $query)) {
	get_template_part('views/partials/content', 'faq',
			[
					'title' => get_field('faq_title', $query),
					'subtitle' => get_field('faq_subtitle', $query),
					'faq' => $faq,
			]);
}
if ($slider = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider',
			[
					'img' => get_field('slider_graph', $query),
					'content' => $slider,
			]);
}
get_footer( 'shop' );
