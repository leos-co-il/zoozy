<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
$fields = get_fields();
$current_id = get_the_ID();
/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
$product_thumb = wp_get_attachment_image_url( get_post_thumbnail_id($current_id), 'full' );;
$product_gallery = $product->get_gallery_image_ids();
$product_gallery_images = [];
if($product_gallery){
	foreach ($product_gallery as $_item){
		$product_gallery_images[] = wp_get_attachment_image_url($_item, 'large', '');
	}
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
	<?php get_template_part('views/partials/repeat', 'top_block',
			[
					'title' => get_the_title(),
					'subtitle' => $fields['product_subtitle'],
			]); ?>
	<div class="container">
		<div class="row justify-content-between pt-4">
			<div class="col-lg-6 col-12">
				<div class="price-total-wrap">
					<?php $sym = get_woocommerce_currency_symbol(); ?>
					<div class="regular-price-title ml-3" data-price="<?= $product->get_regular_price(); ?>" data-sym="<?= $sym; ?>">
						<?= 'לפני הוזלה '.'<s>'.$sym.$product->get_regular_price().'</s>'; ?>
					</div>
					<div class="sale-price-title" data-price_sale="<?= $product->get_sale_price(); ?>" data-sym="<?= $sym; ?>">
						<?= 'מחיר המוצר '.$sym.$product->get_sale_price(); ?>
					</div>
				</div>
				<div class="base-output product-text-output bordered-data">
					<?php do_action( 'display_excerpt_custom' ); ?>
				</div>
				<div class="bordered-data">
					<div class="base-output">
						<h6>
							פרטי ותנאי משלוח
						</h6>
						<?= (isset($fields['delivery_details']) && $fields['delivery_details']) ? $fields['delivery_details'] : opt('delivery_details'); ?>
					</div>
				</div>
				<div class="bordered-data">
					<?php if ($fields['product_file']) : ?>
						<a class="share-item-file" download href="<?= $fields['product_file']['url']; ?>">
							<span class="share-item">
								<img src="<?= ICONS ?>file.png" alt="download-file">
							</span>
							<span class="base-text">
								<?= (isset($fields['product_file']['title']) && $fields['product_file']['title']) ? $fields['product_file']['title'] : 'הורידו את הקובץ'?>
							</span>
						</a>
					<?php endif; ?>
					<div class="share-items-line mt-3">
						<a class="share-item" href="mailto:?&subject=&body=<?= $current_id; ?>">
							<img src="<?= ICONS ?>share-mail.png" alt="share-by-email">
						</a>
						<a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $current_id; ?>" target="_blank"
						   class="share-item">
							<img src="<?= ICONS ?>share-whatsapp.png" alt="share-by-whatsapp">
						</a>
						<span class="base-text">
							שתף את המוצר
					</span>
					</div>
				</div>
				<div class="add-to-card-line">
					<?php
					/**
					 * Hook: woocommerce_single_product_summary.
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 * @hooked WC_Structured_Data::generate_product_data() - 60
					 */
					do_action( 'woocommerce_single_product_summary' );
					?>
				</div>
			</div>
			<?php if ($product_gallery_images || $product_thumb) : ?>
				<div class="col-lg-6 col-12 gallery-main-col mt-lg-0 mt-4 sticky-col-product">
					<div class="sticky-col">
						<?php if ($product_thumb) : ?>
							<div class="gallery-col-pad">
								<div class="gallery-top-item gallery-list-item"
									 style="background-image: url('<?= $product_thumb; ?>')">
									<div class="put-image-here"></div>
								</div>
							</div>
						<?php endif;
						if ($product_gallery_images) : ?>
							<div class="gallery-row">
								<?php foreach ($product_gallery_images as $img): ?>
									<div class="gallery-col gallery-col-product">
										<div class="gallery-list-item photo-gal"
											 style="background-image: url('<?= $img; ?>')" data-url="<?= $img;?>">
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<?php
	/**
	 * Hook: woocommerce_before_single_product_summary.
	 *
	 * @hooked woocommerce_show_product_sale_flash - 10
	 * @hooked woocommerce_show_product_images - 20
	 */
	do_action( 'woocommerce_before_single_product_summary' );
	?>
	<?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action( 'woocommerce_after_single_product_summary' );
	?>
	<?php $post_terms = wp_get_object_terms($current_id, 'product_cat', ['fields' => 'ids']);
	$samePosts = [];
	$samePosts = get_posts([
			'posts_per_page' => 3,
			'post_type' => 'product',
			'post__not_in' => array($current_id),
			'tax_query' => [
					[
							'taxonomy' => 'product_cat',
							'field' => 'term_id',
							'terms' => $post_terms,
					],
			],
	]);
	if ($samePosts === NULL) {
		$samePosts = get_posts([
				'posts_per_page' => 3,
				'orderby' => 'rand',
				'post_type' => 'product',
				'post__not_in' => array($current_id),
		]);
	}
	if ($samePosts) {
		get_template_part('views/partials/content', 'products',
				[
						'posts' => $samePosts,
						'title' => opt('same_products_title_pr') ? opt('same_products_title_pr') : 'מוצרים נוספים',
						'subtitle' => opt('same_products_subtitle_pr') ? opt('same_products_subtitle_pr') : '',
				]);
	}
	get_template_part('views/partials/repeat', 'offer', [
			'text' => true
	]);
	if ($fields['single_slider_seo']) {
		get_template_part('views/partials/content', 'slider', [
				'content' => $fields['single_slider_seo'],
				'img' => $fields['slider_graph'],
		]);
	} ?>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
