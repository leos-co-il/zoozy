<?php $offer_link = (isset($args['link']) && $args['link']) ? $args['link'] : opt('offer_link');
$offer_text = (isset($args['title']) && $args['title']) ? $args['title'] : opt('offer_text');
$offer_text_down = (isset($args['text']) && $args['text']) ? opt('offer_text_down') : '';
$offer_img = (isset($args['img']) && $args['img']) ? $args['img'] : opt('offer_img');
if ($offer_link || $offer_text) : ?>
<section class="offer-back">
	<?php if ($offer_img) : ?>
		<div class="offer-image-block">
			<div class="container h-100">
				<div class="row h-100">
					<div class="col-4 offer-img-col">
						<img src="<?= $offer_img['url']; ?>" alt="offer">
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="offer-wrapper">
					<div class="row justify-content-end">
						<div class="col-sm-8 col-12">
							<div class="row justify-content-between align-items-center col-button-row">
								<?php if ($offer_text_down || $offer_text) : ?>
									<div class="col-xl col-lg-10">
										<h3 class="block-subtitle">
											<?= $offer_text_down; ?>
										</h3>
										<h3 class="block-subtitle subtitle-blue">
											<?= $offer_text; ?>
										</h3>
									</div>
								<?php endif;
								if ($offer_link) : ?>
									<div class="col-xl-auto col-lg-6 col-md-auto col-auto col-button">
										<a href="<?= $offer_link['url'];?>" class="primary-button">
											<?= (isset($offer_link['title']) && $offer_link['title'])
												? $offer_link['title'] : 'התחילו את התהליך';
											?>
										</a>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>
