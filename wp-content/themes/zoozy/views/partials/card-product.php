<?php if (isset($args['product']) && ($args['product'])) : $prod_item = $args['product'];
	$product_all = wc_get_product($prod_item->ID);
	$link = get_permalink($product_all->get_id());
	$id = $product_all->get_id(); ?>
	<div <?php wc_product_class( 'col-md-4 col-post', $product_all ); ?>>
		<?php
		/**
		 * Hook: woocommerce_before_shop_loop_item.
		 *
		 * @hooked woocommerce_template_loop_product_link_open - 10
		 */
		do_action( 'woocommerce_before_shop_loop_item' );
		?>
		<div class="post-card more-card" data-id="<?= $id; ?>">
			<a class="product-card-image" href="<?= $link; ?>">
				<?php if (has_post_thumbnail($args['product'])) : ?>
					<img src="<?= postThumb($args['product']);?>">
				<?php endif; ?>
			</a>
			<div class="post-card-content">
				<span class="price price-title">
					<?= $product_all->get_price_html(); ?>
				</span>
				<a class="post-card-title mt-0" href="<?= $link; ?>">
					<h2 class="woocommerce-loop-product__title"><?= $args['product']->post_title; ?></h2>
				</a>
				<p class="base-text">
					<?= text_preview($product_all->get_short_description(), 10); ?>
				</p>
			</div>
			<a href="<?= $link; ?>" class="base-link post-card-link">
				<?= esc_html__('מעבר למוצר', 'leos'); ?>
			</a>
		</div>
	</div>

<?php endif; ?>


