<?php
$reviews = (isset($args['reviews']) && $args['reviews']) ? $args['reviews'] : '';
if ($reviews) : ?>
	<section class="reviews-block arrows-slider bottom-slider-arrows">
		<div class="container">
			<div class="row justify-content-start">
				<div class="col-auto">
					<h2 class="block-title">
						<?= isset($args['title']) && $args['title'] ? $args['title'] : 'מאמרים נוספים'; ?>
					</h2>
					<?php if (isset($args['subtitle']) && $args['subtitle']) : ?>
						<h3 class="block-subtitle">
							<?= $args['subtitle']; ?>
						</h3>
					<?php endif; ?>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="reviews-slider" dir="rtl">
						<?php foreach ($reviews as $review) :
							$review_img = (isset($review['video']) && $review['video']) ? getYoutubeThumb($review['video']) :
								((isset($review['img']) && $review['img']) ? $review['img']['url'] : ''); ?>
							<div class="p-1">
								<div class="post-card">
									<div class="post-card-image position-relative" <?php if ($review_img) : ?>
											style="background-image: url('<?= $review_img; ?>')"
										<?php endif;?>>
										<?php if (isset($review['video']) && $review['video']) : ?>
											<span class="play-video play-button-slider" data-video="<?= getYoutubeId($review['video']); ?>">
													<img src="<?= ICONS ?>play.png" alt="play-video">
												</span>
										<?php endif; ?>
									</div>
									<div class="post-card-content position-relative">
										<?php if ($review['text']) : ?>
											<div class="rev-pop-trigger">
												<img src="<?= ICONS ?>plus.png" alt="more-review">
												<div class="hidden-review">
													<h3 class="review-title"><?= $review['name']; ?></h3>
													<div class="base-output">
														<?= $review['text']; ?>
													</div>
												</div>
											</div>
										<?php endif; ?>
										<h3 class="review-title"><?= $review['name']; ?></h3>
										<p class="base-text">
											<?= $review['description']; ?>
										</p>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
<div class="modal fade" id="reviewsModal" tabindex="-1" role="dialog"
	 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="form-wrapper position-relative">
			<span type="button" class="close-form" data-dismiss="modal" aria-label="Close">
				<img src="<?= ICONS ?>close.png">
			</span>
			<div class="modal-body" id="reviews-pop-wrapper"></div>
		</div>
	</div>
</div>
<div class="video-modal">
	<div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
		 aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body" id="iframe-wrapper"></div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="close-icon">
								<img src="<?= ICONS ?>close.png">
							</span>
				</button>
			</div>
		</div>
	</div>
</div>

