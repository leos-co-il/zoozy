<div class="top-block">
	<div class="container">
		<div class="row">
			<div class="col">
				<h1 class="block-title"><?= isset($args['title']) ? $args['title'] : ''; ?></h1>
				<h2 class="block-subtitle mb-0"><?= isset($args['subtitle']) ? $args['subtitle'] : ''; ?></h2>
			</div>
		</div>
	</div>
</div>
