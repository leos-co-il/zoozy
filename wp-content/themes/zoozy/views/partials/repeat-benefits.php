<?php if ($benefits = opt('benefits')) : ?>
	<section class="benefits-block">
		<div class="container">
			<?php if ($title = opt('benefits_title')) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<h2 class="block-title text-center mb-3"><?= $title; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-between align-items-center">
				<?php foreach ($benefits as $x => $benefit) : ?>
					<div class="col-md-4 col-12 wow flipInX mb-4" data-wow-delay="0.<?= $x * 2; ?>s">
						<div class="benefit-item">
						<span class="benefit-icon-wrapper">
							<?php if ($benefit['icon']) : ?>
								<img src="<?= $benefit['icon']['url']; ?>" alt="benefit-icon">
							<?php endif; ?>
						</span>
							<h3 class="block-subtitle text-center mb-2"><?= $benefit['title']; ?></h3>
							<p class="base-text text-center"><?= $benefit['desc']; ?></p>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif; ?>
