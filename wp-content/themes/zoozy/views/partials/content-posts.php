<?php if (isset($args['posts']) && $args['posts']) : ?>
	<section class="posts-output">
		<div class="container">
			<div class="row justify-content-start">
				<div class="col-auto">
					<h2 class="block-title">
						<?= isset($args['title']) && $args['title'] ? $args['title'] : 'מאמרים נוספים'; ?>
					</h2>
					<?php if (isset($args['subtitle']) && $args['subtitle']) : ?>
						<h3 class="block-subtitle">
							<?= $args['subtitle']; ?>
						</h3>
					<?php endif; ?>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($args['posts'] as $i => $post) { if (isset($args['products']) && $args['products']) {
					get_template_part('views/partials/card', 'post_product',
							[
									'post' => $post,
							]);
					} else {
					get_template_part('views/partials/card', 'post',
							[
									'post' => $post,
							]);
					}
				} ?>
			</div>
			<?php if (isset($args['link']) && $args['link']) : ?>
				<div class="row justify-content-end">
					<div class="col-auto">
						<a href="<?= $args['link']['url'];?>" class="base-link">
							<?= (isset($args['link']['title']) && $args['link']['title'])
								? $args['link']['title'] : 'לכל המאמרים';
							?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif; ?>
