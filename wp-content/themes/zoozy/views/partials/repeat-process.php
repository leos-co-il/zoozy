<?php if (isset($args['content']) && ($args['content'])) : ?>
	<div class="container pb-3">
		<div class="row justify-content-between align-items-center">
			<?php foreach ($args['content'] as $x => $benefit) : ?>
				<div class="col-md-6 col-12 process-col wow zoomIn" data-wow-delay="0.<?= $x * 2; ?>s">
					<div class="process-item">
						<div class="process-title-wrap">
								<span class="number-wrapper">
									<?= ($x + 1).'.'; ?>
								</span>
							<h3 class="process-title"><?= $benefit['title']; ?></h3>
						</div>
						<p class="base-text process-text"><?= $benefit['desc']; ?></p>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif; ?>
