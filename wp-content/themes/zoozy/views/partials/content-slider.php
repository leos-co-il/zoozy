<?php if (isset($args['content']) && ($args['content'])) :
	$slider_img = isset($args['img']) ? $args['img'] : '';
	$video_url = '';
	$img_url = '';
if ($slider_img) {
	$video_url = ($slider_img[0]['acf_fc_layout'] === 'video') ? $slider_img[0]['url'] : '';
	$img_url = ($slider_img[0]['acf_fc_layout'] === 'image') ? $slider_img[0]['img'] : '';
} ?>
	<div class="base-slider-block arrows-slider arrows-slider-base">
		<div class="container">
			<div class="row justify-content-between align-items-center reverse-slider-row">
				<div class="<?= $video_url || $img_url ? 'col-lg-6 col-12' : 'col-12'; ?> slider-col-content">
					<div class="base-slider" dir="rtl">
						<?php foreach ($args['content'] as $content) : ?>
							<div class="slider-base-item">
								<div class="base-output slider-output">
									<?= $content['content']; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<?php if ($video_url || $img_url) : ?>
					<div class="col-lg-6 col-12 slider-img-col">
						<div class="slider-video-inside" style="background-image: url('<?= $video_url ? getYoutubeThumb($video_url) : $img_url['url']; ?>')">
							<?php if ($video_url) : ?>
								<span class="put-video-here"></span>
								<span class="play-video play-button-slider" data-id="<?= getYoutubeId($video_url); ?>">
									<img src="<?= ICONS ?>play.png" alt="play-video">
								</span>
							<?php endif; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
