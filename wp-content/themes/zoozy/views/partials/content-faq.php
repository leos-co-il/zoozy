<?php if (isset($args['faq']) && $args['faq']) : ?>
	<div class="faq">
		<div class="container">
			<div class="row justify-content-start">
				<div class="col-auto">
					<h2 class="block-title">
						<?= (isset($args['title']) && $args['title']) ? $args['title'] : 'שאלתם ? ענינו !'; ?>
					</h2>
					<?php if (isset($args['subtitle']) && $args['subtitle']) : ?>
						<h3 class="block-subtitle"><?= $args['subtitle']; ?></h3>
					<?php endif; ?>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-12">
					<div id="accordion">
						<?php foreach ($args['faq'] as $num => $item) : ?>
							<div class="card question-card wow fadeInUp" data-wow-delay="0.<?= $num + $i = 1; ?>s" <?php $i++; ?>>
								<div class="question-header" id="heading_<?= $num; ?>">
									<button class="question-title" data-toggle="collapse"
											data-target="#faqChild<?= $num; ?>"
											aria-expanded="false" aria-controls="collapseOne">
										<span class="question-icon">?</span>
										<?= $item['faq_question']; ?>
										<img src="<?= ICONS ?>arrow-faq.png" class="faq-arrow" alt="read-the-answer">
									</button>
									<div id="faqChild<?= $num; ?>" class="collapse faq-item answer-body"
										 aria-labelledby="heading_<?= $num; ?>" data-parent="#accordion">
										<div class="base-output slider-output">
											<?= $item['faq_answer']; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
