<?php if (isset($args['item']) && $args['item']) : $img = $args['item'];
$thumb = (isset($img['video']) && $img['video'] ? getYoutubeThumb($img['video']) : '');
$image = (isset($img['img']) && $img['img']) ? $img['img']['url'] : $thumb;
$video = (isset($img['video']) && $img['video']) ? getYoutubeId($img['video']) : ''; ?>
<div class="col-md-4 col-12 p-md-2 p-3 more-card" data-id="<?= isset($args['num']) ? $args['num'] : ''; ?>">
	<?php if ($image) : ?>
		<div class="gallery-list-item <?= $video ? 'video-gal' : 'photo-gal'; ?>"
			 style="background-image: url('<?= $image; ?>')">
			<?php if ($video) : ?>
				<span class="play-video" data-video="<?= $video; ?>">
					<img src="<?= ICONS ?>play.png" alt="play-video">
				</span>
			<?php endif; ?>
		</div>
	<?php endif; ?>
</div>
<?php endif; ?>
