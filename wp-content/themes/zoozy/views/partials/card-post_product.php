<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-md-4 col-12 col-post">
		<div class="post-card more-card" data-id="<?= $args['post']->ID; ?>">
			<a class="post-card-image" href="<?= $link; ?>"
				<?php if (has_post_thumbnail($args['post'])) : ?>
					style="background-image: url('<?= postThumb($args['post']); ?>')"
				<?php endif;?>>
			</a>
			<div class="post-card-content">
				<p class="price-title"><?php echo 'מחיר '.get_field('service_price_reg', $args['post']->ID).' '.get_woocommerce_currency_symbol(); ?></p>
				<a class="post-card-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
				<p class="base-text">
					<?= text_preview($args['post']->post_content, 10); ?>
				</p>
			</div>
			<a href="<?= $link; ?>" class="base-link post-card-link">
				<?= esc_html__('מעבר למוצר', 'leos'); ?>
			</a>
		</div>
	</div>
<?php endif; ?>
