<?php if ($process_link = opt('header_offer_link')) : ?>
	<a class="primary-button" href="<?= $process_link['url']; ?>">
		<?= (isset($process_link['title']) && $process_link['title']) ? $process_link['title'] : 'התחילו את התהליך'; ?>
	</a>
<?php endif; ?>
