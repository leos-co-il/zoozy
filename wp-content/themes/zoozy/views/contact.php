<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$mail = opt('mail');
$fax = opt('fax');
$address = opt('address');
$open_hours = opt('open_hours');


?>
<article class="article-page-body page-body">
	<?php get_template_part('views/partials/repeat', 'top_block',
		[
			'title' => get_the_title(),
			'subtitle' => $fields['contact_subtitle'],
		]); ?>
	<div class="container pb-4">
		<div class="row justify-content-between">
			<div class="col-12">
				<div class="contact-wrapper">
					<div class="row justify-content-center align-items-center">
						<div class="col-lg-6 col-md-7 col-12">
							<div class="contact-form-block form-wrapper">
								<?php getForm('8'); ?>
							</div>
						</div>
						<div class="col-lg-6 col-md-5 col-12 mt-md-0 mt-4 contact-col">
							<?php if ($tel) : ?>
								<a href="tel:<?= $tel; ?>" class="contact-item wow flipInX" data-wow-delay="0.2s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-tel.png">
									</div>
									<div class="contact-info">
										<h3 class="base-contact-title">דברו איתנו</h3>
										<span class="contact-type"><?= $tel; ?></span>
									</div>
								</a>
							<?php endif; ?>
							<?php if ($mail) : ?>
								<a href="mailto:<?= $mail; ?>" class="contact-item wow flipInX" data-wow-delay="0.4s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-mail.png">
									</div>
									<div class="contact-info">
										<h3 class="base-contact-title">כיתבו לנו</h3>
										<span class="contact-type"><?= $mail; ?></span>
									</div>
								</a>
							<?php endif;
							if ($open_hours || $address) : ?>
								<div  class="contact-item wow flipInX" data-wow-delay="0.6s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-hours.png">
									</div>
									<div class="contact-info">
										<h3 class="base-contact-title">שעות הפעילות שלנו</h3>
										<div class="d-flex flex-wrap align-items-center justify-content-start">
											<span class="contact-type"><?= $open_hours; ?></span>
										</div>
									</div>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
