<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

if ($fields['main_slider']) : ?>
<section class="main-home-block">
	<div class="home-slider" dir="rtl">
		<?php foreach ($fields['main_slider'] as $slide) :
			$img = isset($slide['img']) && isset($slide['img']['url']) ? $slide['img']['url'] : ''; ?>
			<div class="h-100">
				<div class="container h-100">
					<div class="row h-100">
						<?php if ($slide['text']) : ?>
							<div class="<?= $img ? 'col-lg-6' : 'col-12'; ?> d-flex flex-column justify-content-start align-items-start pb80">
								<div class="base-output main-output">
									<?= $slide['text']; ?>
								</div>
								<?php if ($slide['link']) : ?>
									<div class="col-xl-auto col-lg-6 col-md-auto col-auto col-button">
										<a href="<?= $slide['link']['url'];?>" class="primary-button">
											<?= (isset($slide['link']['title']) && $slide['link']['title'])
													? $slide['link']['title'] : 'לשאלון ההתאמה';
											?>
										</a>
									</div>
								<?php endif; ?>
							</div>
						<?php endif;
						if ($img) : ?>
							<div class="<?= $slide['text'] ? 'col-lg-6' : 'col-12'; ?> main-img-col">
								<img src="<?= $img; ?>" alt="slider-image">
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</section>
<?php endif; ?>
<section class="about-home">
	<?php if ($fields['h_about_text'] || $fields['h_about_logo']) : ?>
		<div class="container">
			<div class="row justify-content-between align-items-center">
				<div class="<?= $fields['h_about_logo'] ? 'col-lg-8 col-12' : 'col-12'; ?>">
					<div class="base-output">
						<?= $fields['h_about_text']; ?>
					</div>
					<?php if ($fields['h_about_link']) : ?>
						<div class="row justify-content-start">
							<div class="col-auto">
								<a href="<?= isset($fields['h_about_link']['url']) ? $fields['h_about_link']['url'] : ''; ?>" class="base-link">
									<?= isset($fields['h_about_link']['title']) ? $fields['h_about_link']['title'] : 'עוד עלינו'; ?>
								</a>
							</div>
						</div>
					<?php endif; ?>
				</div>
				<?php if ($fields['h_about_logo']) : ?>
					<div class="col-xl-3 col-lg-4 col-12 about-img-col">
						<img src="<?= $fields['h_about_logo']['url']; ?>" alt="about-us">
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>
</section>
<?php if ($fields['h_about_videos']) : ?>
	<section class="home-video-slider arrows-slider bottom-slider-arrows">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="slider-img-col">
						<div class="video-slider" dir="rtl">
							<?php foreach ($fields['h_about_videos'] as $video) : if (isset($video['video'])) : ?>
								<div>
									<div class="slider-video-inside" style="background-image: url('<?= getYoutubeThumb($video['video']); ?>')">
										<span class="put-video-here"></span>
										<span class="play-video play-button-slider" data-id="<?= getYoutubeId($video['video']); ?>">
											<img src="<?= ICONS ?>play.png" alt="play-video">
										</span>
									</div>
								</div>
							<?php endif; endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['h_process_items'] || $fields['h_process_title'] || $fields['h_process_subtitle']) : ?>
	<section class="process-block-home">
		<div class="container">
			<div class="row">
				<div class="col-auto">
					<?php if ($fields['h_process_title']) : ?>
						<h2 class="block-title"><?= $fields['h_process_title']; ?></h2>
					<?php endif;
					if ($fields['h_process_subtitle']) : ?>
						<h3 class="block-subtitle"><?= $fields['h_process_subtitle']; ?></h3>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php if ($fields['h_process_items']) {
			get_template_part('views/partials/repeat', 'process',
					[
							'content' => $fields['h_process_items'],
					]);
		}
		get_template_part('views/partials/repeat', 'offer',
				[
						'title' => $fields['h_offer_title'],
						'link' => $fields['h_offer_link'],
						'img' => $fields['h_offer_img'],
				]);
		?>
	</section>
<?php endif;
if ($fields['h_reviews']) :
	get_template_part('views/partials/content', 'reviews',
			[
					'title' => $fields['h_reviews_title'],
					'subtitle' => $fields['h_reviews_subtitle'],
					'reviews' => $fields['h_reviews'],
			]);
endif;
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'subtitle' => $fields['faq_subtitle'],
					'faq' => $fields['faq_item'],
			]);
endif; ?>
<section class="home-products-block">
	<?php if ($fields['h_products']) : ?>
		<div class="home-posts">
			<?php get_template_part('views/partials/content', 'products',
					[
							'posts' => $fields['h_products'],
							'title' => $fields['h_products_title'] ? $fields['h_products_title'] : 'מוצרים נוספים',
							'subtitle' => $fields['h_products_desc'] ? $fields['h_products_desc'] : '',
							'link' => $fields['h_products_link']
					]); ?>
		</div>
	<?php endif;
	if ($fields['h_slider_seo']) {
		get_template_part('views/partials/content', 'slider', [
				'content' => $fields['h_slider_seo'],
				'img' => $fields['h_slider_graph'],
		]);
	} ?>
</section>
<?php if ($fields['h_posts']) : ?>
	<div class="home-posts">
		<?php get_template_part('views/partials/content', 'posts',
				[
						'posts' => $fields['h_posts'],
						'title' => $fields['h_posts_title'] ? $fields['h_posts_title'] : 'קראו איתנו',
						'subtitle' => $fields['h_posts_desc'] ? $fields['h_posts_desc'] : '',
						'link' => $fields['h_posts_link']
				]); ?>
	</div>
<?php endif;
if ($fields['single_slider_seo']) : ?>
	<div class="reverse-slider">
		<?php get_template_part('views/partials/content', 'slider', [
				'content' => $fields['single_slider_seo'],
				'img' => $fields['slider_graph'],
		]); ?>
	</div>
<?php endif;
get_footer(); ?>
