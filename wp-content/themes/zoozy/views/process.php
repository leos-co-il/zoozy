<?php
/*
Template Name: שאלון
*/

get_header();
$fields = get_fields();
?>
<article class="article-page-body page-body">
	<?php get_template_part('views/partials/repeat', 'top_block',
		[
			'title' => get_the_title(),
			'subtitle' => $fields['contact_subtitle'],
		]);
	if ($fields['faq_quiz']) : ?>
		<div class="quiz-container">
			<?php foreach ($fields['faq_quiz'] as $x => $quiz) : ?>
				<div class="quiz-item-block">
					<?php if ($x !== 0) : ?>
						<span class="quiz-overlay"></span>
					<?php endif; ?>
					<div class="container">
						<div class="row">
							<div class="col-12">
								<div class="quiz-item <?= $quiz['icon'] ? 'pad-left' : ''; ?>">
									<h2 class="question-quiz"><?= ($x + 1).'. '.$quiz['question']; ?></h2>
									<div class="answer-block">
										<?php if ($x === 0 || $x === 4 || $x === 5) : ?>
											<form class="buttons-group <?= $x === 5 ? 'flex-wrap buttons-group-choice' : ''; ?>" method="post" action="/">
												<label class="answer-button yes <?= $x === 5 ? 'yes-click' : 'scroller'; ?>" for="question-<?= $x + 1; ?>-yes">כן
													<input type="radio" value="yes" data-type="כן" name="question-<?= $x + 1; ?>" placeholder="כן" id="question-<?= $x + 1; ?>-yes"/>
												</label>
												<label class="answer-button no scroller <?= $x === 5 ? 'no-click' : '';?>" for="question-<?= $x + 1; ?>-no">לא
													<input type="radio" value="no" data-type="לא" class="answer-button" name="question-<?= $x + 1; ?>" placeholder="לא" id="question-<?= $x + 1; ?>-no"/>
												</label>
												<?php if ($x === 5) : ?>
													<div class="buttons-group w-100 justify-content-start align-items-stretch line-buttons-area hidden-options">
														<label class="answer-button icon-radio first-area scroller">
															אורטודנטי
															<input type="radio" value="first-area" data-type="אורטודנטי" name="question-option"/>
														</label>
														<label class="answer-button icon-radio second-area scroller">
															דנטלי
															<input type="radio" value="second-area" data-type="דנטלי" class="answer-button" name="question-option"/>
														</label>
														<label class="answer-button icon-radio third-area scroller">
															שיקומי
															<input type="radio" value="third-area" data-type="שיקומי" class="answer-button" name="question-option"/>
														</label>
													</div>
												<?php endif; ?>
											</form>
										<?php elseif ($x === 1 || $x === 2) :
											$other_img = $x === 2; ?>
										<form class="buttons-group" method="post" action="/">
											<label class="answer-button icon-radio first scroller" for="question-<?= $x + 1; ?>-first">
												<img src="<?= IMG ?>first-type<?= $other_img ? '-2' : ''; ?>.png" alt="קל">
												<input type="radio" value="first" name="question-<?= $x + 1; ?>" id="question-<?= $x + 1; ?>-first" data-type="קל"/>
											</label>
											<label class="answer-button icon-radio second scroller" for="question-<?= $x + 1; ?>-second">
												<img src="<?= IMG ?>second-type<?= $other_img ? '-2' : ''; ?>.png" alt="בינוני">
												<input type="radio" value="second" class="answer-button" data-type="בינוני" name="question-<?= $x + 1; ?>" id="question-<?= $x + 1; ?>-second"/>
											</label>
											<label class="answer-button icon-radio third scroller" for="question-<?= $x + 1; ?>-third">
												<img src="<?= IMG ?>third-type<?= $other_img ? '-2' : ''; ?>.png" alt="קשה">
												<input type="radio" value="third" class="answer-button" data-type="קשה" name="question-<?= $x + 1; ?>" id="question-<?= $x + 1; ?>-third"/>
											</label>
											<label class="answer-button icon-radio fourth scroller" for="question-<?= $x + 1; ?>-fourth">ללא
												<input type="radio" value="fourth" class="answer-button" data-type="ללא" name="question-<?= $x + 1; ?>" id="question-<?= $x + 1; ?>-fourth"/>
											</label>
										</form>
										<?php elseif ($x === 3) : ?>
											<form class="buttons-group d-block" method="post" action="/">
												<div class="d-flex justify-content-start align-items-stretch line-buttons-area">
													<label class="answer-button icon-radio first-area scroller" for="question-<?= $x + 1; ?>-first">
														נשיכה עליונה
														<input type="radio" value="first-area" data-type="נשיכה עליונה" name="question-<?= $x + 1; ?>" id="question-<?= $x + 1; ?>-first"/>
													</label>
													<label class="answer-button icon-radio second-area scroller" for="question-<?= $x + 1; ?>-second">
														נשיכה תחתונה
														<input type="radio" value="second-area" data-type="נשיכה תחתונה" class="answer-button" name="question-<?= $x + 1; ?>" id="question-<?= $x + 1; ?>-second"/>
													</label>
													<label class="answer-button icon-radio third-area scroller" for="question-<?= $x + 1; ?>-third">
														נשיכה אלכסונית
														<input type="radio" value="third-area" data-type="נשיכה אלכסונית" class="answer-button" name="question-<?= $x + 1; ?>" id="question-<?= $x + 1; ?>-third"/>
													</label>
												</div>
												<textarea class="textarea-form w-100" name="textarea-faq" placeholder="הערה :"></textarea>
											</form>
										<?php elseif ($x === 6) : ?>
											<form class="buttons-group d-block" method="post" action="/">
												<div class="checkbox-group">
													<input type="checkbox" value="תוצאות מהירות" name="question-<?= $x + 1; ?>" id="question-<?= $x + 1; ?>-1"/>
													<label class="checkbox-label-custom" for="question-<?= $x + 1; ?>-1">
														תוצאות מהירות
													</label>
												</div>
												<div class="checkbox-group">
													<input type="checkbox" value="מחיר נמוך" name="question-<?= $x + 1; ?>" id="question-<?= $x + 1; ?>-1"/>
													<label class="checkbox-label-custom" for="question-<?= $x + 1; ?>-1">
														מחיר נמוך
													</label>
												</div>
												<div class="checkbox-group">
													<input type="checkbox" value="איכות המוצר" name="question-<?= $x + 1; ?>" id="question-<?= $x + 1; ?>-1"/>
													<label class="checkbox-label-custom" for="question-<?= $x + 1; ?>-1">
														איכות המוצר
													</label>
												</div>
												<div class="checkbox-group">
													<input type="checkbox" value="כל התשובות נכונות" name="question-<?= $x + 1; ?>" id="question-<?= $x + 1; ?>-1"/>
													<label class="checkbox-label-custom" for="question-<?= $x + 1; ?>-1">
														כל התשובות נכונות
													</label>
												</div>
											</form>
										<?php endif; ?>
									</div>
									<?php if ($quiz['icon']) : ?>
										<div class="quiz-icon">
											<img src="<?= $quiz['icon']['url']; ?>" alt="icon-quiz">
										</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
	<div class="process-form-block">
		<div class="container">
			<div class="row justify-content-start">
				<div class="col-xxl-6 col-xl-8 col-lg-6 col-12">
					<div class="form-wrapper">
						<?php if ($fields['process_form_title']) : ?>
							<h2 class="block-title"><?= $fields['process_form_title']; ?></h2>
						<?php endif;
						getForm('267'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="process-back">
			<?php if ($fields['process_form_img']) : ?>
				<div class="container">
					<div class="row justify-content-end h-100">
						<div class="col-lg-6 col-form-img h-100">
							<img src="<?= $fields['process_form_img']['url']; ?>" alt="finish-process">
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php if ($fields['reviews']) :
	get_template_part('views/partials/content', 'reviews',
			[
					'title' => $fields['reviews_title'],
					'subtitle' => $fields['reviews_subtitle'],
					'reviews' => $fields['reviews'],
			]);
endif;
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'subtitle' => $fields['faq_subtitle'],
					'faq' => $fields['faq_item'],
			]);
endif;
get_footer(); ?>
