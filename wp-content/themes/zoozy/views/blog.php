<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$posts = new WP_Query([
	'posts_per_page' => 9,
	'post_type' => 'post',
	'suppress_filters' => false
]);
$published_posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => 'post',
	'suppress_filters' => false,
]);
?>
<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'top_block',
		[
			'title' => get_the_title(),
			'subtitle' => $fields['contact_subtitle'],
			]); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<?php foreach ($posts->posts as $post) {
					get_template_part('views/partials/card', 'post',
						[
							'post' => $post,
						]);
				} ?>
			</div>
		<?php endif;
		if ($published_posts->have_posts() && (($num = count($published_posts->posts)) > 9)) : ?>
			<div class="row justify-content-center mt-4">
				<div class="col-auto">
					<div class="more-link load-more-posts" data-type="post" data-count="<?= $num; ?>">
						<?= esc_html__('טען עוד מאמרים', 'leos'); ?>
						<img src="<?= ICONS ?>arrow-down.png" alt="down-arrow">
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'subtitle' => $fields['faq_subtitle'],
			'faq' => $fields['faq_item'],
		]);
endif;
if ($fields['single_slider_seo']) : ?>
	<div class="reverse-slider">
		<?php get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_graph'],
		]); ?>
	</div>
<?php endif;
get_footer(); ?>

