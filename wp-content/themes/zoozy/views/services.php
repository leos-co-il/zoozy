<?php
/*
Template Name: שירותים
*/

get_header();
$fields = get_fields();
$posts = new WP_Query([
	'posts_per_page' => 9,
	'post_type' => 'service',
	'suppress_filters' => false
]);
$published_posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => 'service',
	'suppress_filters' => false,
]);
?>
<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'top_block',
		[
			'title' => get_the_title(),
			'subtitle' => $fields['contact_subtitle'],
		]); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row justify-content-end">
				<div class="col-auto">
					<form method="post" id="order-custom">
						<select name="orderby" class="select-sort">
							<option disabled selected>מיין לפי </option>
							<option value="date" name="newest">למיין לפי המעודכן ביותר</option>
							<option value="title" name="title">מיין לפי שם</option>
							<option value="price" name="price">למיין מהזול ליקר</option>
							<option value="price-desc" name="price-desc">למיין מהיקר לזול</option>
						</select>
					</form>
				</div>
			</div>
			<div class="row align-items-stretch put-here-posts put-here-products justify-content-center">
				<?php foreach ($posts->posts as $post) {
					get_template_part('views/partials/card', 'post_product',
						[
							'post' => $post,
						]);
				} ?>
			</div>
		<?php endif;
		if ($published_posts->have_posts() && (($num = count($published_posts->posts)) > 9)) : ?>
			<div class="row justify-content-center mt-4">
				<div class="col-auto">
					<div class="more-link load-more-posts" data-type="service" data-count="<?= $num; ?>">
						<?= esc_html__('טען עוד מוצרים', 'leos'); ?>
						<img src="<?= ICONS ?>arrow-down.png" alt="down-arrow">
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'subtitle' => $fields['faq_subtitle'],
			'faq' => $fields['faq_item'],
		]);
endif;
if ($fields['single_slider_seo']) : ?>
	<div class="reverse-slider">
		<?php get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_graph'],
		]); ?>
	</div>
<?php endif;
get_footer(); ?>

