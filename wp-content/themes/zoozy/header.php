<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>

<?php
$offer_link = opt('header_offer_link');
$contact_id = getPageByTemplate('views/contact.php');
$process_id = getPageByTemplate('views/process.php');
$current = get_the_ID();
$contact = $current === $contact_id;
$process = $current === $process_id;
$blog = getPageByTemplate('views/blog.php') === $current;
?>
<header class="sticky">
    <div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-xl-9">
				<div class="row align-items-center justify-content-between">
					<div class="col-auto d-flex justify-content-start align-items-center">
						<?php if ((is_front_page() || is_product() || $contact || !$process || $blog) && $offer_link) : ?>
							<a href="<?= $offer_link['url'];?>" class="primary-button">
								<?= (isset($offer_link['title']) && $offer_link['title'])
										? $offer_link['title'] : 'לתהליך ההתאמה';
								?>
							</a>
						<?php endif; ?>
						<?php if (!is_front_page() && !$blog) : ?>
						<a href="<?= wc_get_cart_url(); ?>" class="cart-btn-header">
							<img src="<?= ICONS ?>basket.png" alt="cart">
						</a>
						<?php endif; ?>
						<a class="user-link" href="<?= wp_login_url(); ?>">
							<img src="<?= ICONS ?>user.png" alt="login">
						</a>
						<nav id="MainNav" class="h-100">
							<div id="MobNavBtn">
								<span></span>
								<span></span>
								<span></span>
							</div>
							<?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
						</nav>
					</div>
					<?php if ($logo = opt('logo_header')) : ?>
						<div class="col-auto">
							<a href="/" class="logo">
								<img src="<?= $logo['url'] ?>" alt="logo">
							</a>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
    </div>
</header>

<div class="pop-trigger">
	<img src="<?= ICONS ?>popup.png" alt="contact-us">
</div>

<div class="pop-great">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-sm-auto col-12">
				<div class="float-form">
					<div class="form-wrapper">
						<span class="close-form">
							<img src="<?= ICONS ?>close.png" alt="close-form">
						</span>
						<div class="pop-form">
							<?php if ($popTitle = opt('pop_form_title')) : ?>
								<h2 class="form-title">
									<?= $popTitle; ?>
								</h2>
							<?php endif;
							if ($popSubtitle = opt('pop_form_subtitle')) : ?>
								<h2 class="form-subtitle">
									<?= $popSubtitle; ?>
								</h2>
							<?php endif; ?>
							<?php getForm('7'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
