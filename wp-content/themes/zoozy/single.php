<?php

the_post();
get_header();
$fields = get_fields();
$current_id = get_the_ID();
$post_gallery = $fields['service_gallery'];
?>
<article class="article-page-body page-body">
	<?php get_template_part('views/partials/repeat', 'top_block',
		[
			'title' => get_the_title(),
			'subtitle' => $fields['product_subtitle'],
		]); ?>
	<div class="container">
		<div class="row justify-content-between">
			<div class="col-lg-7 col-12 d-flex flex-column align-items-start">
				<div class="base-output post-text-output">
					<?php the_content(); ?>
				</div>
				<?php if ($fields['product_file']) : ?>
					<a class="share-item-file" download href="<?= $fields['product_file']['url']; ?>">
							<span class="share-item">
								<img src="<?= ICONS ?>file.png" alt="download-file">
							</span>
						<span class="base-text">
								<?= (isset($fields['product_file']['title']) && $fields['product_file']['title']) ? $fields['product_file']['title'] : 'הורידו את הקובץ'?>
							</span>
					</a>
				<?php endif; ?>
				<div class="share-items-line mt-3">
					<a class="share-item" href="mailto:?&subject=&body=<?= $current_id; ?>">
						<img src="<?= ICONS ?>share-mail.png" alt="share-by-email">
					</a>
					<a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $current_id; ?>" target="_blank"
					   class="share-item">
						<img src="<?= ICONS ?>share-whatsapp.png" alt="share-by-whatsapp">
					</a>
					<span class="base-text">
							שתף את המאמר
						</span>
				</div>
			</div>
			<?php if ($post_gallery || has_post_thumbnail()) : ?>
				<div class="col-lg-5 col-12 gallery-main-col mt-lg-0 mt-4">
					<div class="sticky-col">
						<?php if (has_post_thumbnail()) : ?>
							<div class="gallery-col-pad">
								<div class="gallery-top-item gallery-list-item"
									 style="background-image: url('<?= postThumb(); ?>')">
									<div class="put-image-here"></div>
									<span class="put-video-here"></span>
								</div>
							</div>
						<?php endif;
						if ($post_gallery) : ?>
							<div class="gallery-row">
								<?php foreach ($post_gallery as $img):
									$thumb = (isset($img['video']) && $img['video'] ? getYoutubeThumb($img['video']) : '');
									$image = (isset($img['img']) && $img['img']) ? $img['img']['url'] : $thumb;
									$video = (isset($img['video']) && $img['video']) ? getYoutubeId($img['video']) : ''; ?>
									<div class="gallery-col">
										<?php if ($image) : ?>
											<div class="gallery-list-item <?= $video ? 'video-gal' : 'photo-gal'; ?>"
												 style="background-image: url('<?= $image; ?>')" data-url="<?= $image;?>">
												<?php if ($video) : ?>
													<span class="play-video play-button-slider" data-id="<?= $video; ?>">
													<img src="<?= ICONS ?>play.png" alt="play-video">
												</span>
												<?php endif; ?>
											</div>
										<?php endif; ?>
									</div>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>

<?php if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'subtitle' => $fields['faq_subtitle'],
					'faq' => $fields['faq_item'],
			]);
endif;
get_template_part('views/partials/repeat', 'offer', [
		'text' => true
]);
$post_terms = wp_get_object_terms($current_id, 'category', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
		'posts_per_page' => 3,
		'post_type' => 'post',
		'post__not_in' => array($current_id),
		'tax_query' => [
				[
						'taxonomy' => 'category',
						'field' => 'term_id',
						'terms' => $post_terms,
				],
		],
]);
if ($samePosts === NULL) {
	$samePosts = get_posts([
			'posts_per_page' => 3,
			'orderby' => 'rand',
			'post_type' => 'post',
			'post__not_in' => array($current_id),
	]);
}
if ($samePosts) {
	get_template_part('views/partials/content', 'posts',
			[
					'posts' => $samePosts,
					'title' => opt('same_posts_title') ? opt('same_posts_title') : 'קראו איתנו',
					'subtitle' => opt('same_posts_subtitle') ? opt('same_posts_subtitle') : '',
			]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_graph'],
	]);
}
get_footer(); ?>
