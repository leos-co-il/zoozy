<?php

the_post();
get_header();
$fields = get_fields();
$current_id = get_the_ID();
$post_gallery = $fields['service_gallery'];
?>
<article class="article-page-body page-body service-page">
	<div class="product-service_block">
		<?php if ($fields['service_process']) {
			get_template_part('views/partials/repeat', 'process',
					[
							'content' => $fields['service_process'],
					]);
		} ?>
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-lg-7 col-12 d-flex flex-column align-items-start">
					<h1 class="block-title"><?php the_title(); ?></h1>
					<div class="prices-wrap">
						<?php if ($fields['service_price_reg']) : ?>
							<p class="service-price-regular"><?= $fields['service_price_reg']; ?></p>
						<?php endif;
						if ($fields['service_price_sale']) : ?>
							<p class="service-price-sale">
								<?= $fields['service_price_sale']; ?>
							</p>
						<?php endif; ?>
					</div>
					<div class="base-output post-text-output">
						<?php the_content(); ?>
					</div>
					<?php if ($tel = opt('tel_sales')) : ?>
						<a class="product-tel-link mb-3" href="tel:<?= $tel; ?>">
							<span class="block-subtitle">צרו איתנו קשר</span>
							<span class="tel-num"><?= $tel; ?></span>
						</a>
					<?php endif;
					get_template_part('views/partials/repeat', 'button_start');
					if ($fields['product_file']) : ?>
						<a class="share-item-file mt-4" download href="<?= $fields['product_file']['url']; ?>">
							<span class="share-item">
								<img src="<?= ICONS ?>file.png" alt="download-file">
							</span>
							<span class="base-text">
								<?= (isset($fields['product_file']['title']) && $fields['product_file']['title']) ? $fields['product_file']['title'] : 'הורידו את הקובץ'?>
							</span>
						</a>
					<?php endif; ?>
					<div class="share-items-line mt-3">
						<a class="share-item" href="mailto:?&subject=&body=<?= $current_id; ?>">
							<img src="<?= ICONS ?>share-mail.png" alt="share-by-email">
						</a>
						<a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $current_id; ?>" target="_blank"
						   class="share-item">
							<img src="<?= ICONS ?>share-whatsapp.png" alt="share-by-whatsapp">
						</a>
						<span class="base-text">
							שתף את המוצר
						</span>
					</div>
				</div>
				<?php if ($post_gallery || has_post_thumbnail()) : ?>
					<div class="col-lg-5 col-12 arrows-slider mt-lg-0 mt-4 slider-gallery-service">
						<div class="gallery-slider" dir="rtl">
							<?php if(has_post_thumbnail()): ?>
								<div class="p-1">
									<a class="big-slider-item" style="background-image: url('<?= postThumb(); ?>')"
									   href="<?= postThumb(); ?>" data-lightbox="images"></a>
								</div>
							<?php endif;
							foreach ($post_gallery as $img):
								$thumb = (isset($img['video']) && $img['video'] ? getYoutubeThumb($img['video']) : '');
								$image = (isset($img['img']) && $img['img']) ? $img['img']['url'] : $thumb;
								$video = (isset($img['video']) && $img['video']) ? getYoutubeId($img['video']) : ''; ?>
								<div class="p-1">
									<?php if ($video) : ?>
										<div class="big-slider-item" style="background-image: url('<?= $image; ?>')">
											<span class="play-video play-button-slider" data-video="<?= $video; ?>">
												<img src="<?= ICONS ?>play.png" alt="play-video">
											</span>
										</div>
									<?php else : ?>
										<a class="big-slider-item" style="background-image: url('<?= $image; ?>')"
										   href="<?= $image; ?>" data-lightbox="images">
										</a>
									<?php endif; ?>
								</div>
							<?php endforeach; ?>
						</div>
						<div class="thumbs" dir="rtl">
							<?php if(has_post_thumbnail()): ?>
								<div class="p-1">
									<a class="thumb-item" style="background-image: url('<?= postThumb(); ?>')"
									   href="<?= postThumb(); ?>" data-lightbox="images-small"></a>
								</div>
							<?php endif;
							foreach ($post_gallery as $img):
								$thumb = (isset($img['video']) && $img['video'] ? getYoutubeThumb($img['video']) : '');
								$image = (isset($img['img']) && $img['img']) ? $img['img']['url'] : $thumb;
								$video = (isset($img['video']) && $img['video']) ? getYoutubeId($img['video']) : ''; ?>
								<div class="p-1">
									<?php if ($video) : ?>
										<div class="thumb-item" style="background-image: url('<?= $image; ?>')"
										   href="<?= $image; ?>" data-lightbox="images-small">
										<span class="play-video play-button-slider" data-video="<?= $video; ?>">
													<img src="<?= ICONS ?>play.png" alt="play-video">
												</span>
										</div>
									<?php else : ?>
										<a class="thumb-item" style="background-image: url('<?= $image; ?>')"
										   href="<?= $image; ?>" data-lightbox="images-small">
										</a>
									<?php endif; ?>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php get_template_part('views/partials/repeat', 'benefits');
	if ($fields['service_reviews']) : $count = count($fields['service_reviews']); ?>
		<section class="service-review-block">
			<div class="container">
				<?php if ($fields['service_reviews_title']) : ?>
					<div class="row justify-content-center">
						<div class="col-auto">
							<h2 class="block-title"><?= $fields['service_reviews_title']; ?></h2>
						</div>
					</div>
				<?php endif; ?>
				<div class="row align-items-stretch put-here-posts">
					<?php foreach ($fields['service_reviews'] as $x => $img) {
						if ($x < 6) {
							get_template_part('views/partials/card', 'gallery',
									[
											'item' => $img,
											'num' => $x,
									]);
						}
					} ?>
				</div>
				<?php if ($count > 6) : ?>
					<div class="row justify-content-center mt-4">
						<div class="col-auto">
							<div class="more-link load-more-posts" data-type="gallery" data-page="<?= get_queried_object_id(); ?>" data-count="<?= $count; ?>">
								טען עוד תוצאות
								<img src="<?= ICONS ?>arrow-down.png" alt="load-more">
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</section>
		<div class="video-modal">
			<div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
				 aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-body" id="iframe-wrapper"></div>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="close-icon">
								<img src="<?= ICONS ?>close.png">
							</span>
						</button>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>

<?php if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'subtitle' => $fields['faq_subtitle'],
					'faq' => $fields['faq_item'],
			]);
endif;
get_template_part('views/partials/repeat', 'offer');
$post_terms = wp_get_object_terms($current_id, 'service_cat', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
		'posts_per_page' => 3,
		'post_type' => 'service',
		'post__not_in' => array($current_id),
		'tax_query' => [
				[
						'taxonomy' => 'service_cat',
						'field' => 'term_id',
						'terms' => $post_terms,
				],
		],
]);
if ($samePosts == NULL) {
	$samePosts = get_posts([
			'posts_per_page' => 3,
			'orderby' => 'rand',
			'post_type' => 'service',
			'post__not_in' => array($current_id),
	]);
}
if ($samePosts) {
	get_template_part('views/partials/content', 'posts',
			[
					'posts' => $samePosts,
					'products' => true,
					'title' => opt('same_products_title') ? opt('same_products_title') : 'מוצרים נוספים',
					'subtitle' => opt('same_products_subtitle') ? opt('same_products_subtitle') : '',
			]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_graph'],
	]);
}
get_footer(); ?>
