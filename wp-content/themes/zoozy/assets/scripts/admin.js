'use strict';
(function($){
	$(function(){
		const el = $('#wpcf7-form')
		if( el.length ) {
			var editorSettings = wp.codeEditor.defaultSettings ? _.clone( wp.codeEditor.defaultSettings ) : {};
			editorSettings.codemirror = _.extend(
				{},
				editorSettings.codemirror,
				{
					indentUnit: 2,
					tabSize: 2,
					mode : "xml",
					htmlMode: true,
				}
			);
			var editor = wp.codeEditor.initialize( el, editorSettings );
		}
		$('#postbox-container-1').append('<a id="load-template" class="button-primary">Load template</a>');

		$(document).on('click', '#load-template', function() {

			const form = '<div class="form-row">\n' +
				'\t<div class="col-md-6 col-12">\n' +
				'\t\t<div class="control-wrap">\n' +
				'\t\t\n' +
				'\t\t</div>\n' +
				'\t</div>\n' +
				'\t<div class="col-md-6 col-12">\n' +
				'\t\t<div class="control-wrap">\n' +
				'\t\t\n' +
				'\t\t</div>\n' +
				'\t</div>\n' +
				'\t<div class="col-12">\n' +
				'\t\t<div class="submit-wrap">\n' +
				'\t\t\n' +
				'\t\t</div>\n' +
				'\t</div>\n' +
				'</div>';


			editor.codemirror.setValue(form)
		});
	});
})(jQuery);
