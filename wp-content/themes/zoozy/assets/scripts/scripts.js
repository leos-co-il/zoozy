(function($) {
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
	$('.pop-great').click(function() {
		$('.float-form').removeClass('show-float-form');
		$('.pop-great').removeClass('show-pop');
	});
	$('.pop-great').click(function(event){
		event.stopPropagation();
	});
	$('input[type="radio"]').change(function() {
		var value = $(this).val();
		$(this).parent('.answer-button').parent('.buttons-group').children('.answer-button').removeClass('active');
		if ($(this).parent('.answer-button').hasClass(value)) {
			$(this).parent('.answer-button').addClass('active');
		}
		else {
			$('.answer-button').removeClass('active');
		}
	});
	$('input:checkbox[name="question-7"]').each(function(i, obj) {
		$(obj).click(function () {
			$(this).toggleClass('checked');
		});
	});
	$( document ).ready(function() {
		var textareaText = window.localStorage.getItem('question-4-text');
		$('textarea[name="textarea-faq"]').val(textareaText);
		$('input[name="question-1"]').change(function() {
			var value1 = $(this).data('type');
			localStorage.setItem('question-1', value1);
		});
		$('input[name="question-2"]').change(function() {
			var value2 = $(this).data('type');
			console.log(value2);
			localStorage.setItem('question-2', value2);
		});
		$('input[name="question-3"]').change(function() {
			var value3 = $(this).data('type');
			localStorage.setItem('question-3', value3);
		});
		$('input[name="question-4"]').change(function() {
			var value4 = $(this).data('type');
			localStorage.setItem('question-4', value4);
		});
		$('textarea[name="textarea-faq"]').change(function () {
			var valueText = $(this).val();
			localStorage.setItem('question-4-text', valueText);
		});
		$('input[name="question-5"]').change(function() {
			var value5 = $(this).data('type');
			localStorage.setItem('question-5', value5);
		});
		$('input[name="question-6"]').change(function() {
			var value6 = $(this).data('type');
			localStorage.setItem('question-6', value6);
		});
		$('input[name="question-option"]').change(function() {
			var valueOption = $(this).data('type');
			localStorage.setItem('question-6-option', valueOption);
		});

		//VALUE CHECKBOXES
		$('.process-form-block form').submit(function(){
			var answer1 = window.localStorage.getItem('question-1');
			var answer2 = window.localStorage.getItem('question-2');
			var answer3 = window.localStorage.getItem('question-3');
			var answer4 = window.localStorage.getItem('question-4');
			var answer4Text = window.localStorage.getItem('question-4-text');
			var answer5 = window.localStorage.getItem('question-5');
			var answer6 = window.localStorage.getItem('question-6');
			var answer6Option = window.localStorage.getItem('question-6-option');
			var value7;
			$('input:checkbox[name="question-7"]').each(function(i, obj) {
				value7 += $(obj).val() + ',';
				localStorage.setItem('question-7', value7);
			});
			var answer7 = window.localStorage.getItem('question-7');
			$('#answer-1').val(answer1);
			$('#answer-2').val(answer2);
			$('#answer-3').val(answer3);
			$('#answer-4').val(answer4 + ',' + answer4Text);
			$('#answer-5').val(answer5);
			$('#answer-6').val(answer6 + ',' + answer6Option);
			$('#answer-7').val(answer7);

		});
		$('.scroller').click(function () {
			var cls = $(this).closest('.quiz-item-block').offset().top;
			$(this).closest('.quiz-item-block').next().addClass('showed-item');
			$('html, body').animate({scrollTop: cls}, 'slow');
		});
		$('.yes-click').click(function () {
			$(this).parent('.buttons-group-choice').children('.line-buttons-area').removeClass('hidden-options');
		});
		$('.no-click').click(function () {
			$(this).parent('.buttons-group-choice').children('.line-buttons-area').addClass('hidden-options');
		});
		$( '.rev-pop-trigger' ).click( function() {
			var contentAll = $( this ).children('.hidden-review').html();
			$( '#reviews-pop-wrapper' ).html( contentAll );
			$( '#reviewsModal' ).modal( 'show' );
		});
		$( '#reviewsModal' ).on( 'hidden.bs.modal', function( e ) {
			$( '#reviews-pop-wrapper' ).html( '' );
		});
		$('.pop-trigger').click(function () {
			$('.pop-great').toggleClass('show-pop');
			$('.float-form').toggleClass('show-float-form');
		});
		$('.close-pop').click(function () {
			$('.pop-great').removeClass('show-pop');
			$('.float-form').removeClass('show-float-form');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$('.video-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$('.home-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: false,
			dots: false,
		});
		$('.gallery-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			rtl: true,
			asNavFor: '.thumbs'
		});
		$('.thumbs').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			rtl: true,
			asNavFor: '.gallery-slider',
			dots: false,
			arrows: true,
			responsive: [
				{
					breakpoint: 1600,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				// {
				// 	breakpoint: 768,
				// 	settings: {
				// 		slidesToShow: 2,
				// 	}
				// },
			]
		});
		$('.reviews-slider').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1600,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-video').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		$('.play-button-slider').click(function() {
			var id = $(this).data('id');
			$('.put-image-here').removeClass('show-img');
			var iFrame = '<iframe src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('.put-video-here').addClass('show').html(iFrame);
		});
		var accordion = $('#accordion');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.question-title').children('.faq-arrow').addClass('swap');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.question-title').children('.faq-arrow').removeClass('swap');
		});
		var prodAcc = $('#accordion-product');
		prodAcc.on('shown.bs.collapse', function (e) {
			$('#accordion-product .collapse.show').parent().addClass('active');
		});

		prodAcc.on('hide.bs.collapse', function (e) {
			$('#accordion-product .collapse.show').parent().removeClass('active');
		});
		$('.photo-gal').click(function() {
			var img = $(this).data('url');
			$('.put-image-here').css('background-image', 'url(' + img + ')').addClass('show-img');
		});
		//Socials
		$('.trigger-wrap').hover(function(){
			$(this).children('.all-socials').addClass('show-share');
		}, function(){
			$(this).children('.all-socials').removeClass('show-share');
		});
	});

	$('.load-more-posts').click(function(e) {
		e.preventDefault();
		var btn = $(this);
		btn.addClass('loading');
		btn.append('<div class="cart-loading"><i class="fas fa-spinner fa-pulse"></i></div>');
		var postType = $(this).data('type');
		var termID = $(this).data('term');
		var parent = $(this).data('parent');
		var termName = $(this).data('term_name');
		// var params = $('.take-json').html();
		var ids = '';
		var page = $(this).data('page');
		var countAll = $(this).data('count');
		var quantity = $('.more-card').length;
		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				postType: postType,
				termID: termID,
				parent: parent,
				termName: termName,
				ids: ids,
				page: page,
				quantity: quantity,
				countAll: countAll,
				// taxType: taxType,
				action: 'get_more_function',
			},
			success: function (data) {
				btn.removeClass('loading');
				$('.cart-loading').remove();
				if (!data.html || data.quantity) {
					btn.addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});
	// //More products
	// var button = $( '#loadmore a' );
	// var paged = button.data( 'paged' );
	// var	maxPages = button.data( 'maxpages' );
	// var textLoad = button.data('loading');
	// var textLoadMore = button.data('load');
	//
	// button.click( function( event ) {
	//
	// 	event.preventDefault();
	// 	var ids = '';
	// 	$('.more-prod').each(function(i, obj) {
	// 		ids += $(obj).data('id') + ',';
	// 	});
	// 	$.ajax({
	// 		type : 'POST',
	// 		dataType: 'json',
	// 		url: '/wp-admin/admin-ajax.php',
	// 		data : {
	// 			paged : paged,
	// 			ids: ids,
	// 			action : 'loadmore'
	// 		},
	// 		beforeSend : function() {
	// 			button.text(textLoad);
	// 		},
	// 		success : function( data ){
	//
	// 			paged++;
	// 			$('.put-here-prods').append(data.html);
	// 			button.text(textLoadMore);
	//
	// 			if( paged === maxPages ) {
	// 				button.remove();
	// 			}
	// 			if (!data.html) {
	// 				$('.more-link').addClass('hide');
	// 			}
	// 		}
	//
	// 	});
	//
	// } );
	var termID = $('#order-custom').data('id');
	console.log(termID);
	$('#order-custom').on('change', function(e) {
		e.preventDefault();
		var orderType = $(this).find(':selected').val();
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				orderType: orderType,
				termID: termID,
				action: 'order_function',
			},
			success: function (data) {
				$('.put-here-products').html(data.html);
			}
		});
	});
})( jQuery );
