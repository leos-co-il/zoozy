<?php

the_post();
get_header();
$fields = get_fields();

?>

<article class="py-5">
	<div class="container">
		<div class="row">
			<div class="col">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</article>

<?php get_footer(); ?>
