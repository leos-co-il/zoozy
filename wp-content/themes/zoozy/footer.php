<?php

$tel = opt('tel');
$mail = opt('mail');
$fax = opt('fax');
$address = opt('address');
$open_hours = opt('open_hours');
$current_id = get_queried_object_id();
$contact_id = getPageByTemplate('views/contact.php');
$logo = opt('logo');
$id = get_the_ID();
?>

<footer>
	<div class="footer-main">
		<div class="footer-form">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<div class="form-wrapper-foo">
							<?php if ($title = opt('foo_form_title')) : ?>
								<h3 class="block-subtitle"><?= $title; ?></h3>
							<?php endif;
							getForm('6'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<a id="go-top">
				<span class="go-top-wrap">
					<img src="<?= ICONS ?>to-top-arrow.png" alt="go-top" class="go-top-arrow">
				</span>
			<h5 class="to-top-text">
				למעלה
			</h5>
		</a>
		<div class="container footer-container-menu">
			<div class="row justify-content-center">
				<div class="col-lg-2 col-md-3 col-6 foo-menu">
					<h3 class="foo-title">ניווט</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-menu', '1'); ?>
					</div>
				</div>
				<div class="col-md col-12 foo-menu foo-menu-links">
					<h3 class="foo-title"><?= opt('foo_menu_title'); ?></h3>
					<div class="menu-border-top">
						<?php getMenu('footer-links-menu', '1', 'two-columns'); ?>
					</div>
				</div>
				<div class="col-md-3 col-6 foo-contact-menu">
					<h3 class="foo-title">דרכי התקשרות</h3>
					<div class="menu-border-top">
						<ul class="contact-list">
							<?php  if ($address) : ?>
								<li>
									<a href="https://www.waze.com/ul?q=<?= $address; ?>" class="foo-link">
										<?= $address; ?>
									</a>
								</li>
							<?php endif;
							if ($tel) : ?>
								<li>
									<a href="tel:<?= $tel; ?>" class="foo-link">
										<?= $tel; ?>
									</a>
								</li>
							<?php endif;
							if ($fax) : ?>
								<li>
									<div class="foo-link fax-item">
										<?= $fax; ?>
									</div>
								</li>
							<?php endif;
							if ($mail) : ?>
								<li>
									<a href="mailto:<?= $mail; ?>" class="foo-link">
										<?= $mail; ?>
									</a>
								</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
			<div class="row justify-content-end">
				<?php if ($logo) : ?>
					<div class="col-auto">
						<div class="logo-footer">
							<a href="/" class="logo">
								<img src="<?= $logo['url'] ?>" alt="logo">
							</a>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
